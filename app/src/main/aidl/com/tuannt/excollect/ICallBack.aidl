// ICallBack.aidl
package com.tuannt.excollect;

// Declare any non-default types here with import statements

interface ICallBack {
   void onResult(String message);
}
