package com.tuannt.excollect;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Comment
 *
 * @author TuanNT
 */
public class Student implements Parcelable {
    public String name;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.name);
    }

    public Student() {
    }

    protected Student(Parcel in) {
        this.name = in.readString();
    }

    public static final Creator<Student> CREATOR = new Creator<Student>() {
        public Student createFromParcel(Parcel source) {
            return new Student(source);
        }

        public Student[] newArray(int size) {
            return new Student[size];
        }
    };
}
