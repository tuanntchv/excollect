package com.tuannt.excollect.sensor;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.tuannt.excollect.R;

/**
 * Comment
 *
 * @author TuanNT
 */
public class AccelerometerActivity extends AppCompatActivity implements SensorEventListener {
    private final String TAG = this.getClass().getSimpleName();
    private SensorManager mSensorManager;
    private Sensor mSensorAccelerometer;
    private Sensor mSensorGyroscope;

    private TextView mTvX;
    private TextView mTvY;
    private TextView mTvZ;
    private TextView mTvX2;
    private TextView mTvY2;
    private TextView mTvZ2;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_accelerometer);

        mTvX = (TextView) findViewById(R.id.mTvX);
        mTvY = (TextView) findViewById(R.id.mTvY);
        mTvZ = (TextView) findViewById(R.id.mTvZ);
        mTvX2 = (TextView) findViewById(R.id.mTvX2);
        mTvY2 = (TextView) findViewById(R.id.mTvY2);
        mTvZ2 = (TextView) findViewById(R.id.mTvZ2);

        initSensor();
    }

    private void initSensor() {
        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        mSensorAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        mSensorGyroscope = mSensorManager.getDefaultSensor(Sensor.TYPE_GRAVITY);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mSensorManager.registerListener(this, mSensorAccelerometer, SensorManager.SENSOR_DELAY_UI);
        mSensorManager.registerListener(this, mSensorGyroscope, SensorManager.SENSOR_DELAY_UI);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mSensorManager.unregisterListener(this);
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        if (sensorEvent.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
            mTvX.setText(sensorEvent.values[0] + "");
            mTvY.setText(sensorEvent.values[1] + "");
            mTvZ.setText(sensorEvent.values[2] + "");
        } else {
            mTvX2.setText(sensorEvent.values[0] + "");
            mTvY2.setText(sensorEvent.values[1] + "");
            mTvZ2.setText(sensorEvent.values[2] + "");
        }
//        Log.d(TAG, "onSensorChanged: x" + sensorEvent.values[0] + " y-" + sensorEvent.values[1] + " z-" + sensorEvent.values[2]);
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }
}
