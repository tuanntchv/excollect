package com.tuannt.excollect.provider;

import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.tuannt.excollect.R;

/**
 * Comment
 *
 * @author TuanNT
 */
public class ProvidersActivity extends AppCompatActivity{
    private Button btnRetrieve;
    private Button btnAdd;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_provider);
        btnRetrieve = (Button)findViewById(R.id.btnRetrieve);
        btnAdd = (Button)findViewById(R.id.btnAdd);


        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClickAddName();
            }
        });
        btnRetrieve.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClickRetrieveStudents();
            }
        });
    }

    public void onClickAddName() {
        ContentValues values = new ContentValues();

        values.put(StudentsProvider.NAME, ((EditText) findViewById(R.id.txtName)).getText().toString());
        values.put(StudentsProvider.GRADE, ((EditText) findViewById(R.id.txtGrade)).getText().toString());

        Uri uri = Uri.parse("content://com.tuannt.provider.College/students");
        Uri uri2 = getContentResolver().insert(uri, values);

        Toast.makeText(getBaseContext(), uri.toString(), Toast.LENGTH_LONG).show();
    }

    public void onClickRetrieveStudents() {
        String URI = "content://com.tuannt.provider.College/students";
        Uri students = Uri.parse(URI);
        Cursor c = managedQuery(students, null, null, null, "name");
        if (c.moveToFirst()) {
            do {
                Log.d("result", "onClickRetrieveStudents: "+ c.getString(c.getColumnIndex(StudentsProvider.NAME)));
            } while (c.moveToNext());
        }
    }
}
