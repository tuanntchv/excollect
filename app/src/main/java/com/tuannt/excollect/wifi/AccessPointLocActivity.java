package com.tuannt.excollect.wifi;

import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.TextView;

import com.tuannt.excollect.R;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Comment
 *
 * @author TuanNT
 */
public class AccessPointLocActivity extends AppCompatActivity {
    private static final String TAG = AccessPointLocActivity.class.getSimpleName();

    WifiManager wifiManager;

    TextView mTvDistance;
    Handler mHandler;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_access_point_loc);
 //       wifiManager = (WifiManager) getSystemService(Context.WIFI_SERVICE);

        mTvDistance = (TextView) findViewById(R.id.mTvDistance);
        mHandler = new Handler();
        //    mHandler.postDelayed(runnable, 500);

        AccelerateDecelerateInterpolator interpolator = new AccelerateDecelerateInterpolator();
        for (int i = 0; i < 20; i++) {
            Log.d(TAG, "run: " + i + "/" + interpolator.getInterpolation(i));
        }
    }


    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            // TODO: 1/26/16 get frequency
            ScanResult scanResul = null;
//            int i = scanResul.frequency;
//            scanResul.level  frequency
            int f = wifiManager.getConnectionInfo().getFrequency();

            mTvDistance.setText(calculateDistance(wifiManager.getConnectionInfo().getRssi(), f) + "");
            Log.d(TAG, "run: " + f);
            mHandler.postDelayed(this, 500);


        }
    };

    public double calculateDistance(double signalLevelInDb, double freqInMHz) {
        double exp = (27.55 - (20 * Math.log10(freqInMHz)) + Math.abs(signalLevelInDb)) / 20.0;
        return Math.pow(10.0, exp);
    }

    private final static ArrayList<Integer> channelsFrequency = new ArrayList<Integer>(
            Arrays.asList(0, 2412, 2417, 2422, 2427, 2432, 2437, 2442, 2447,
                    2452, 2457, 2462, 2467, 2472, 2484));

    public static Integer getFrequencyFromChannel(int channel) {
        return channelsFrequency.get(channel);
    }

    public static int getChannelFromFrequency(int frequency) {
        return channelsFrequency.indexOf(Integer.valueOf(frequency));
    }
}
