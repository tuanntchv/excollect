package com.tuannt.excollect.realm;

import android.util.Log;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Comment
 *
 * @author TuanNT
 */
public class User extends RealmObject {
    private static final String TAG = User.class.getSimpleName();

    @PrimaryKey
    private int id;
    private String name;
    private int age;
    private int tall;

    public User() {
    }

    public static List<User> getUsers() {
        return Realm.getDefaultInstance().where(User.class).findAll();
    }

    public void save() {
        Realm.getDefaultInstance().executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.copyToRealmOrUpdate(User.this);
            }
        }, new Realm.Transaction.OnSuccess() {
            @Override
            public void onSuccess() {
                Log.d(TAG, "onSuccess: ");
            }
        });
    }

    public User(int id, String name, int age, int tall) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.tall = tall;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getTall() {
        return tall;
    }

    public void setTall(int tall) {
        this.tall = tall;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
