package com.tuannt.excollect.realm;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Comment
 *
 * @author TuanNT
 */
public class Car extends RealmObject{
    @PrimaryKey
    private int id;
    private String color;
}
