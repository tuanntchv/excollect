package com.tuannt.excollect.realm;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.tuannt.excollect.BaseActivity;
import com.tuannt.excollect.R;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;

/**
 * Comment
 *
 * @author TuanNT
 */
public class RealmActivity extends BaseActivity {
    private static final String TAG = RealmActivity.class.getSimpleName();
    private Button mBtnSave;
    private Handler mHandler = new Handler();

    private List<User> mUsers = new ArrayList<>();
    private int count = 1;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_realm);
        mBtnSave = (Button) findViewById(R.id.mBtnSave);

        initUsers();

        mBtnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(TAG, "onClick: ");

                Realm.getDefaultInstance().executeTransactionAsync(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        for (User user : mUsers) {
                            realm.copyToRealmOrUpdate(user);
                        }
                    }
                }, new Realm.Transaction.OnSuccess() {
                    @Override
                    public void onSuccess() {
                        Log.d(TAG, "onSuccess: " + User.getUsers().size());
                        mHandler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                Log.d(TAG, "run: " + User.getUsers().size());
                                initUsers();
                            }
                        }, 1000);
                    }
                });

            }
        });
    }

    private void initUsers() {
        for (int i = 0; i < 200; i++) {
            User user = new User();
            user.setId(i* count);
            user.setName("name " + System.currentTimeMillis());
            user.setAge(i + 1);
            user.setTall(i);
            mUsers.add(user);
        }
        count++;
    }
}
