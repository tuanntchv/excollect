package com.tuannt.excollect.intent;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.tuannt.excollect.BaseActivity;
import com.tuannt.excollect.R;

/**
 * Comment
 *
 * @author TuanNT
 */
public class SendIntentActivity extends BaseActivity {
    private TextView mTvCustomScheme;
    private Button mBtnSendText;
    private Button mBtnSendImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_intent);
        mTvCustomScheme = (TextView) findViewById(R.id.mTvCustomScheme);

        mBtnSendText = (Button) findViewById(R.id.mBtnSendText);
        mBtnSendImage = (Button) findViewById(R.id.mBtnSendImage);

        mBtnSendText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendText();
            }
        });
        mBtnSendImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendImage();
            }
        });
    }

    private void sendImage() {
        Uri uriToImage = Uri.parse("android.resource://com.tuannt.excollect/" + R.drawable.ic_launcher);
        Intent shareIntent = new Intent();
        shareIntent.setAction(Intent.ACTION_SEND);
        shareIntent.putExtra(Intent.EXTRA_STREAM, uriToImage);
        shareIntent.putExtra("abc", "hello");
        shareIntent.setType("image/png");
        startActivity(shareIntent);
    }

    private void sendText() {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, "This is my text to send.");
        sendIntent.setType("text/plain");
        startActivity(sendIntent);
    }
}
