package com.tuannt.excollect.intent;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.tuannt.excollect.R;

/**
 * Comment
 *
 * @author TuanNT
 */
public class ReceiveSendActivity extends AppCompatActivity {
    private final String TAG = this.getClass().getSimpleName();
    private TextView mTvResultText;
    private ImageView mImgResultImage;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_receive_send);
        mTvResultText = (TextView) findViewById(R.id.mTvResultText);
        mImgResultImage = (ImageView) findViewById(R.id.mImgResultImage);

        Intent receiveIntent = getIntent();
        if (receiveIntent != null) {
            String action = receiveIntent.getAction();
            if (action.equals(Intent.ACTION_VIEW)) {
                Log.d(TAG, "onCreate: action view");
                receiveText(receiveIntent);

            } else if (action.equals(Intent.ACTION_SEND)) {
                Log.d(TAG, "onCreate: action send");
                String type = receiveIntent.getType();
                if (type.startsWith("text/")) {
                    Log.d(TAG, "onCreate: type= text");
                    receiveText(receiveIntent);
                } else if (type.startsWith("image/")) {
                    Log.d(TAG, "onCreate: type = image");
                    receiveImage(receiveIntent);
                }
            } else {
                Log.d(TAG, "onCreate: action main");
            }
        } else {
            Log.d(TAG, "onCreate: receive null");
        }

    }

    private void receiveText(Intent intent) {
        mTvResultText.setText(null);
        String receivedText = intent.getStringExtra(Intent.EXTRA_TEXT);
        mTvResultText.setText(receivedText);
    }

    private void receiveImage(Intent intent) {
        mTvResultText.setText(null);
        Uri receivedUri = intent.getParcelableExtra(Intent.EXTRA_STREAM);
        String receiveString = intent.getStringExtra("abc");
        mTvResultText.setText(receiveString);
        mImgResultImage.setImageURI(receivedUri);
    }


}
