package com.tuannt.excollect.wfp2p;

import android.net.wifi.p2p.WifiP2pDevice;

/**
 * Comment
 *
 * @author TuanNT
 */
public class Peer {
    private String name;
    private WifiP2pDevice device;

    public Peer() {
    }

    public Peer(String name, WifiP2pDevice device) {
        this.name = name;
        this.device = device;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public WifiP2pDevice getDevice() {
        return device;
    }

    public void setDevice(WifiP2pDevice device) {
        this.device = device;
    }
}
