package com.tuannt.excollect.wfp2p;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tuannt.excollect.R;

import java.util.List;

/**
 * Comment
 *
 * @author TuanNT
 */
public class PeerAdapter extends RecyclerView.Adapter {
    public interface OnClientItemClick {
        void onItemClick(int position);
    }

    private List<Peer> peers;
    private OnClientItemClick mListener;

    public PeerAdapter(List<Peer> clients, OnClientItemClick l) {
        this.peers = clients;
        this.mListener = l;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_peer, parent, false);
        return new MessageHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((MessageHolder) holder).tvMessage.setText(peers.get(position).getName());
    }

    @Override
    public int getItemCount() {
        return peers.size();
    }

    private class MessageHolder extends RecyclerView.ViewHolder {
        private final TextView tvMessage;

        public MessageHolder(View itemView) {
            super(itemView);
            tvMessage = (TextView) itemView;
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mListener.onItemClick(getLayoutPosition());
                }
            });
        }
    }

    public void setmListener(OnClientItemClick mListener) {
        this.mListener = mListener;
    }
}
