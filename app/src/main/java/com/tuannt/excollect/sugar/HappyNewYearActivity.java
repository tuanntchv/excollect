package com.tuannt.excollect.sugar;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.tuannt.excollect.R;

import java.util.List;

/**
 * Comment
 *
 * @author TuanNT
 */
public class HappyNewYearActivity extends AppCompatActivity {
    private final String TAG = this.getClass().getSimpleName();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hpny);

        List<Category> categorys = Category.listAll(Category.class);
        for (Category category : categorys) {
            Log.d(TAG, "onCreate: " + category.getName());
        }

        String arg = String.valueOf(1);
        List<Category> category = Category.find(Category.class, "ID = ?", arg);

        List<Sms> smses = Sms.listAll(Sms.class);
        for (Sms sms :smses)
        {
            Log.d("aaa",""+sms.getCategoryId());
            Log.d("aaa",""+sms.getText());
        }

    }
}
