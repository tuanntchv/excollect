package com.tuannt.excollect.sugar;

import com.orm.SugarRecord;
import com.orm.dsl.Column;
import com.orm.dsl.Table;

/**
 * Comment
 *
 * @author TuanNT
 */
@Table(name = "SMS")
public class Sms extends SugarRecord {
    @Column(name = "category_id", notNull = true)
    private int categoryId;
    @Column(name = "sText", notNull = true)
    private String text;

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Sms(int categoryId, String text) {
        this.categoryId = categoryId;
        this.text = text;
    }

    public Sms() {

    }
}
