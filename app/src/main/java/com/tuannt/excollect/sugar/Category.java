package com.tuannt.excollect.sugar;

import com.orm.SugarRecord;
import com.orm.dsl.Column;
import com.orm.dsl.Table;

/**
 * Comment
 *
 * @author TuanNT
 */
@Table(name = "Category")
public class Category extends SugarRecord {
    @Column(name = "cName", notNull = true)
    private String name;

    public Category(String name) {
        this.name = name;
    }

    public Category() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
