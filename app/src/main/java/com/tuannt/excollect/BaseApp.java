package com.tuannt.excollect;

import android.content.Context;
import android.content.ContextWrapper;
import android.database.sqlite.SQLiteDatabase;

import com.facebook.FacebookSdk;
import com.orm.SugarApp;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Comment
 *
 * @author TuanNT
 */
public class BaseApp extends SugarApp {
    @Override
    public void onCreate() {
        super.onCreate();
        initDB();
        FacebookSdk.sdkInitialize(this);

        Realm.init(this);
        RealmConfiguration realmConfig = new RealmConfiguration
                .Builder()
                .deleteRealmIfMigrationNeeded()
                .build();
        Realm.setDefaultConfiguration(realmConfig);
    }

    private void initDB() {
        try {
            if (!doesDatabaseExist(this, "/data/data/com.tuannt.excollect/databases/cool_msm.db")) {
                Context context = getApplicationContext();
                SQLiteDatabase db = context.openOrCreateDatabase("cool_msm.db", context.MODE_PRIVATE, null);
                db.close();
                InputStream dbInput = getApplicationContext().getAssets().open("cool_msm.db");
                String outFileName = "/data/data/com.tuannt.excollect/databases/cool_msm.db";
                OutputStream dbOutput = new FileOutputStream(outFileName);
                try {
                    byte[] buffer = new byte[1024];
                    int length;
                    while ((length = dbInput.read(buffer)) > 0) {
                        dbOutput.write(buffer, 0, length);
                    }
                } finally {
                    dbOutput.flush();
                    dbOutput.close();
                    dbInput.close();
                }
            }
        } catch (Exception e) {
            e.toString();
        }
    }

    private boolean doesDatabaseExist(ContextWrapper context, String dbName) {
        File dbFile = context.getDatabasePath(dbName);
        return dbFile.exists();
    }
}
