package com.tuannt.excollect.maps.models;

import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.clustering.ClusterItem;

/**
 * Comment
 *
 * @author TuanNT
 */
public class ClusterObject implements ClusterItem {
    private final LatLng mPosition;

    public ClusterObject(double lat, double lng) {
        mPosition = new LatLng(lat, lng);
    }

    @Override
    public LatLng getPosition() {
        return mPosition;
    }
}
