package com.tuannt.excollect.maps;

import com.tuannt.excollect.maps.models.ClusterObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Comment
 *
 * @author TuanNT
 */
public class JsonReader {
    private static final String REGEX_INPUT_BOUNDARY_BEGINNING = "\\A";

    public List<ClusterObject> read(InputStream inputStream) throws JSONException {
        List<ClusterObject> items = new ArrayList<ClusterObject>();
        String json = new Scanner(inputStream).useDelimiter(REGEX_INPUT_BOUNDARY_BEGINNING).next();
        JSONArray array = new JSONArray(json);
        for (int i = 0; i < array.length(); i++) {
            JSONObject object = array.getJSONObject(i);
            double lat = object.getDouble("lat");
            double lng = object.getDouble("lng");
            items.add(new ClusterObject(lat, lng));
        }
        return items;
    }
}
