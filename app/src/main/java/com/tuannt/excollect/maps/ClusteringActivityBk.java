package com.tuannt.excollect.maps;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.clustering.ClusterManager;
import com.tuannt.excollect.R;
import com.tuannt.excollect.maps.models.ClusterObject;

import org.json.JSONException;

import java.io.InputStream;
import java.util.List;

/**
 * Comment
 *
 * @author TuanNT
 */
public class ClusteringActivityBk extends AppCompatActivity {
    private GoogleMap mMap;
    private ClusterManager<ClusterObject> mClusterManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map_clustering);
        setUpMapIfNeeded();

        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(51.503186, -0.126446), 10));

        try {
            initCluster();
        } catch (JSONException e) {
            Log.d("result",e.getMessage());
        }
    }

    private void initCluster() throws JSONException {
        mClusterManager = new ClusterManager<>(this, mMap);
        InputStream inputStream = getResources().openRawResource(R.raw.radar_search);
        List<ClusterObject> items = new JsonReader().read(inputStream);
        mClusterManager.addItems(items);
        mMap.setOnCameraChangeListener(mClusterManager);
    }

    private void setUpMapIfNeeded() {
        if (mMap == null) {
            mMap = ((SupportMapFragment) getSupportFragmentManager()
                    .findFragmentById(R.id.mMap)).getMap();
            if (mMap != null) {
                mMap.getUiSettings().setMapToolbarEnabled(true);
                mMap.getUiSettings().setMyLocationButtonEnabled(true);
                mMap.setMyLocationEnabled(true);
/*
                mMap.setOnMapClickListener(this);
                mMap.setOnMarkerClickListener(this);
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(mCity.getLatLng(), mCity.getZoomLevel()));
*/
            }
        }
    }
}
