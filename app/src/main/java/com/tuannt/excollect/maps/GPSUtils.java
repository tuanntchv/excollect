package com.tuannt.excollect.maps;

import android.content.Context;
import android.location.Location;
import android.location.LocationManager;

import com.google.android.gms.maps.model.LatLng;

/**
 * Comment
 *
 * @author TuanNT
 */
public final class GPSUtils {

    private GPSUtils() {
    }

    public static boolean canGetLocation(Context context) {
        return networkProviderEnable(context) || gpsProviderEnable(context);
    }

    private static boolean networkProviderEnable(Context context) {
        LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        return locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
    }

    private static boolean gpsProviderEnable(Context context) {
        LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    public static LatLng getLocation(Context context) {
        LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        LatLng resultLocation = null;

        if (networkProviderEnable(context)) {
            Location location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            if(location != null){
                resultLocation = new LatLng(location.getLatitude(), location.getLongitude());
                return resultLocation;
            }
        }

        if (gpsProviderEnable(context)) {
            Location location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            if(location != null){
                resultLocation = new LatLng(location.getLatitude(), location.getLongitude());
                return resultLocation;
            }
        }
        return resultLocation;
    }
}
