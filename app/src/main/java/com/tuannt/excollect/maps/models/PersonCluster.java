package com.tuannt.excollect.maps.models;

import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.clustering.ClusterItem;

/**
 * Comment
 *
 * @author TuanNT
 */
public class PersonCluster implements ClusterItem{
    public final String name;
    public final int profilePhoto;
    private final LatLng mPosition;

    public PersonCluster(LatLng position, String name, int pictureResource) {
        this.name = name;
        profilePhoto = pictureResource;
        mPosition = position;
    }

    @Override
    public LatLng getPosition() {
        return mPosition;
    }
}
