package com.tuannt.excollect.maps;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.tuannt.excollect.R;

/**
 * Comment
 *
 * @author TuanNT
 */
public class MapActivity extends AppCompatActivity implements GoogleMap.OnMapClickListener, GoogleMap.OnMarkerClickListener, GoogleMap.OnMarkerDragListener {
    private static final LatLng PERTH = new LatLng(-31.90, 115.86);
    private static final String TAG = MapActivity.class.getName();

    private GoogleMap mMap;
    private Marker mMoveMarker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);

        setUpMapIfNeeded();

        mMap.setOnMapClickListener(this);
        mMap.setOnMarkerClickListener(this);
        mMap.setOnMarkerDragListener(this);

        mMoveMarker = mMap.addMarker(new MarkerOptions()
                .position(PERTH)
                .title("move")
                .draggable(true));

        mMap.moveCamera(CameraUpdateFactory.newLatLng(PERTH));

    }

    private void setUpMapIfNeeded() {
        if (mMap == null) {
            mMap = ((SupportMapFragment) getSupportFragmentManager()
                    .findFragmentById(R.id.mMap)).getMap();
            if (mMap != null) {
                mMap.getUiSettings().setMapToolbarEnabled(true);
                mMap.getUiSettings().setMyLocationButtonEnabled(true);
                mMap.setMyLocationEnabled(true);

//                mMap.setOnMapClickListener(this);
//                mMap.setOnMarkerClickListener(this);

//                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(mCity.getLatLng(), mCity.getZoomLevel()));
            }
        }
    }

    @Override
    public void onMapClick(LatLng latLng) {

        mMap.addMarker(new MarkerOptions()
                .draggable(true)
                .position(new LatLng(latLng.latitude, latLng.longitude))
                .title("Hello world"));
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        Log.d(TAG, "onMarkerClick: " + marker.getTitle());

        return false;
    }

    @Override
    public void onMarkerDragStart(Marker marker) {
        Log.d(TAG, "onMarkerDragStart: " + marker.getTitle());
    }

    @Override
    public void onMarkerDrag(Marker marker) {
        Log.d(TAG, "onMarkerDrag: " + marker.getTitle());
    }

    @Override
    public void onMarkerDragEnd(Marker marker) {
        Log.d(TAG, "onMarkerDragEnd: " + marker.getTitle());
    }
}
