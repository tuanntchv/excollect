package com.tuannt.excollect.maps;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.clustering.Cluster;
import com.google.maps.android.clustering.ClusterManager;
import com.tuannt.excollect.R;
import com.tuannt.excollect.maps.models.PersonCluster;

import org.json.JSONException;

import java.util.Random;

/**
 * Comment
 *
 * @author TuanNT
 */
public class CustomClusteringActivity extends AppCompatActivity implements
        ClusterManager.OnClusterClickListener<PersonCluster>,
        ClusterManager.OnClusterInfoWindowClickListener<PersonCluster>,
        ClusterManager.OnClusterItemClickListener<PersonCluster>,
        ClusterManager.OnClusterItemInfoWindowClickListener<PersonCluster> {
    private final String TAG = this.getClass().getSimpleName();
    private GoogleMap mMap;
    private ClusterManager<PersonCluster> mClusterManager;
    private Random mRandom = new Random(1984);

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map_clustering_custom);
        setUpMapIfNeeded();

        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(51.503186, -0.126446), 10));

        try {
            initCluster();
        } catch (JSONException e) {
            Log.d("result", e.getMessage());
        }
    }

    private void initCluster() throws JSONException {
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(51.503186, -0.126446), 9.5f));

        mClusterManager = new ClusterManager<>(this, mMap);
        mClusterManager.setRenderer(new PersonClusterRender(this, mMap, mClusterManager));
        mMap.setOnCameraChangeListener(mClusterManager);
        mMap.setOnMarkerClickListener(mClusterManager);
        mMap.setOnInfoWindowClickListener(mClusterManager);
        mClusterManager.setOnClusterClickListener(this);
        mClusterManager.setOnClusterInfoWindowClickListener(this);
        mClusterManager.setOnClusterItemClickListener(this);
        mClusterManager.setOnClusterItemInfoWindowClickListener(this);

        addItems();
        mClusterManager.cluster();
    }

    private void addItems() {
        // http://www.flickr.com/photos/sdasmarchives/5036248203/
        mClusterManager.addItem(new PersonCluster(position(), "Walter", R.drawable.walter));

        // http://www.flickr.com/photos/usnationalarchives/4726917149/
        mClusterManager.addItem(new PersonCluster(position(), "Gran", R.drawable.gran));

        // http://www.flickr.com/photos/nypl/3111525394/
        mClusterManager.addItem(new PersonCluster(position(), "Ruth", R.drawable.ruth));

        // http://www.flickr.com/photos/smithsonian/2887433330/
        mClusterManager.addItem(new PersonCluster(position(), "Stefan", R.drawable.stefan));

        // http://www.flickr.com/photos/library_of_congress/2179915182/
        mClusterManager.addItem(new PersonCluster(position(), "Mechanic", R.drawable.mechanic));

        // http://www.flickr.com/photos/nationalmediamuseum/7893552556/
        mClusterManager.addItem(new PersonCluster(position(), "Yeats", R.drawable.yeats));

        // http://www.flickr.com/photos/sdasmarchives/5036231225/
        mClusterManager.addItem(new PersonCluster(position(), "John", R.drawable.john));

        // http://www.flickr.com/photos/anmm_thecommons/7694202096/
        mClusterManager.addItem(new PersonCluster(position(), "Trevor the Turtle", R.drawable.turtle));

        // http://www.flickr.com/photos/usnationalarchives/4726892651/
        mClusterManager.addItem(new PersonCluster(position(), "Teach", R.drawable.teacher));
    }

    private LatLng position() {
        return new LatLng(random(51.6723432, 51.38494009999999), random(0.148271, -0.3514683));
    }

    private double random(double min, double max) {
        return mRandom.nextDouble() * (max - min) + min;
    }

    private void setUpMapIfNeeded() {
        if (mMap == null) {
            mMap = ((SupportMapFragment) getSupportFragmentManager()
                    .findFragmentById(R.id.mMap)).getMap();
            if (mMap != null) {
                mMap.getUiSettings().setMapToolbarEnabled(true);
                mMap.getUiSettings().setMyLocationButtonEnabled(true);
                mMap.setMyLocationEnabled(true);
/*
                mMap.setOnMapClickListener(this);
                mMap.setOnMarkerClickListener(this);
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(mCity.getLatLng(), mCity.getZoomLevel()));
*/
            }
        }
    }

    @Override
    public boolean onClusterClick(Cluster<PersonCluster> cluster) {
        Log.d(TAG, "onClusterClick: ");
        return false;
    }

    @Override
    public void onClusterInfoWindowClick(Cluster<PersonCluster> cluster) {
        Log.d(TAG, "onClusterInfoWindowClick: ");
    }

    @Override
    public boolean onClusterItemClick(PersonCluster personCluster) {
        Log.d(TAG, "onClusterItemClick: " + personCluster.name);
        return false;
    }

    @Override
    public void onClusterItemInfoWindowClick(PersonCluster personCluster) {
        Log.d(TAG, "onClusterItemInfoWindowClick: " + personCluster.name);

    }
}
