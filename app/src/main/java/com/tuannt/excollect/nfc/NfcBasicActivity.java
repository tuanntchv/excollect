package com.tuannt.excollect.nfc;

import android.content.Intent;
import android.nfc.NfcAdapter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.tuannt.excollect.R;

/**
 * Comment
 *
 * @author TuanNT
 */
public class NfcBasicActivity extends AppCompatActivity {
    //    http://code.tutsplus.com/tutorials/sharing-files-with-nfc-on-android--cms-22501
//    http://www.jessechen.net/blog/how-to-nfc-on-the-android-platform/
    private NfcAdapter mNfcAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nfc);

        mNfcAdapter = NfcAdapter.getDefaultAdapter(this);
        if (mNfcAdapter == null) {
            // Stop here, we definitely need NFC
            Toast.makeText(this, "This device doesn't support NFC.", Toast.LENGTH_LONG).show();
            finish();
            return;
        }

        if (!mNfcAdapter.isEnabled()) {
            Toast.makeText(NfcBasicActivity.this, "nfc is not enable", Toast.LENGTH_SHORT).show();
        } else {
            handlerIntent(getIntent());
        }

    }

    private void handlerIntent(Intent intent) {
    }
}
