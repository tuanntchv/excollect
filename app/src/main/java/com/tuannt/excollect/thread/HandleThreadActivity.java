package com.tuannt.excollect.thread;

import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.tuannt.excollect.R;

import java.util.concurrent.TimeUnit;

/**
 * Comment
 *
 * @author TuanNT
 */
public class HandleThreadActivity extends AppCompatActivity {
    private final String TAG = this.getClass().getSimpleName();
    private Button mBtnStartHandleThread;
    private HandlerThread handlerThread;
    private WorkerThread mWorkerThread;

    private Handler handlerMessage2;
    private HandlerThread handlerThread2;

    private Handler handlerMessage;

    private Handler mUiHandler = new Handler();

    int count = 1;

    @Override
    protected void onCreate(@Nullable final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_handle_thread);
        mBtnStartHandleThread = (Button) findViewById(R.id.mBtnStartHandleThread);

        handlerThread = new HandlerThread("T");
        handlerThread.start();
        handlerMessage = new Handler(handlerThread.getLooper()) {
            @Override
            public void handleMessage(Message msg) {
                try {
                    TimeUnit.SECONDS.sleep(2);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                Log.d(TAG, "handleMessage: " + msg.getData());
            }
        };


        handlerThread2 = new HandlerThread("T2");
        handlerThread2.start();
        handlerMessage2 = new Handler(handlerThread2.getLooper()) {
            @Override
            public void handleMessage(Message msg) {
                try {
                    TimeUnit.SECONDS.sleep(2);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                Log.d(TAG, "handleMessage: " + msg.getData());
            }
        };


        mBtnStartHandleThread.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                startBaseEx();
                //    start();
                // startSendMessage();
//                if (count <= 3) {
//                    startRunable();
//                    count++;
//                } else {
//                    handlerThread.quitSafely();
//                }
                //   startRunable();
                runMultil();
            }
        });
    }

    private void runMultil() {
        handlerMessage.post(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < 100; i++) {
                    Log.d(TAG, "run: " + handlerThread.getId() + "->>>" + i);
                }
            }
        });

        handlerMessage2.post(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < 100; i++) {
                    Log.d(TAG, "run: " + handlerThread2.getId() + "->>>" + i);
                }
            }
        });

    }


    private void startRunable() {
        handlerMessage.post(new Runnable() {
            @Override
            public void run() {
                try {
                    TimeUnit.SECONDS.sleep(2);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                Log.d(TAG, "startRunable: ");
            }
        });
    }

    private void startSendMessage() {
        Message message = handlerMessage.obtainMessage();
        Bundle b = new Bundle();
        b.putInt("id", 1);
        message.setData(b);
        handlerMessage.sendMessage(message);
    }


    private void start() {
        mWorkerThread = new WorkerThread("myWorkerThread");
        Runnable task = new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < 4; i++) {
                    try {
                        TimeUnit.SECONDS.sleep(2);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    if (i == 2) {
                        mUiHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(HandleThreadActivity.this,
                                        "I am at the middle of background task",
                                        Toast.LENGTH_LONG)
                                        .show();
                            }
                        });
                    }
                }
                mUiHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(HandleThreadActivity.this,
                                "Background task is completed",
                                Toast.LENGTH_LONG)
                                .show();
                    }
                });
            }
        };
        mWorkerThread.start();
        mWorkerThread.prepareHandler();
        mWorkerThread.postTask(task);
        mWorkerThread.postTask(task);
    }

    private void startBaseEx() {

        Thread myThread = new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < 4; i++) {
                    try {
                        TimeUnit.SECONDS.sleep(2);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    if (i == 2) {
                        mUiHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(HandleThreadActivity.this, "I am at the middle of background task", Toast.LENGTH_LONG).show();
                            }
                        });
                    }
                }
                mUiHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(HandleThreadActivity.this, "Background task is completed", Toast.LENGTH_LONG).show();
                    }
                });
            }
        });
        myThread.start();
    }
}
