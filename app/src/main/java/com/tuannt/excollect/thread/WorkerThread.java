package com.tuannt.excollect.thread;

import android.os.Handler;
import android.os.HandlerThread;

/**
 * Comment
 *
 * @author TuanNT
 */
public class WorkerThread extends HandlerThread {
    private Handler mHandler;

    public WorkerThread(String name) {
        super(name);
    }

    public void postTask(Runnable runnable) {
        mHandler.post(runnable);
    }

    public void prepareHandler() {
        mHandler = new Handler(getLooper());
    }
}
