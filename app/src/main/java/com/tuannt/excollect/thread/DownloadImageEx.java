package com.tuannt.excollect.thread;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.tuannt.excollect.R;

import java.util.Random;

/**
 * Comment
 *
 *  http://blog.nikitaog.me/2014/10/18/android-looper-handler-handlerthread-ii/
 *
 * @author TuanNT
 */
public class DownloadImageEx extends AppCompatActivity implements MyWorkerThread.Callback{
    private static boolean isVisible;
    public static final int LEFT_SIDE = 0;
    public static final int RIGHT_SIDE = 1;
    private LinearLayout mLeftSideLayout;
    private LinearLayout mRightSideLayout;
    private MyWorkerThread mWorkerThread;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_download_image);

        isVisible = true;
        mLeftSideLayout = (LinearLayout) findViewById(R.id.mLeftSideLayout);
        mRightSideLayout = (LinearLayout) findViewById(R.id.mRightSideLayout);
        String[] urls = new String[]{"http://developer.android.com/design/media/principles_delight.png",
                "http://developer.android.com/design/media/principles_real_objects.png",
                "http://developer.android.com/design/media/principles_make_it_mine.png",
                "http://developer.android.com/design/media/principles_get_to_know_me.png"};
        mWorkerThread = new MyWorkerThread(new Handler(), this);
        mWorkerThread.start();
        mWorkerThread.prepareHandler();
        Random random = new Random();
        for (String url : urls){
            mWorkerThread.queueTask(url, random.nextInt(2), new ImageView(this));
        }
    }
    @Override
    protected void onPause() {
        isVisible = false;
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        mWorkerThread.quit();
        super.onDestroy();
    }

    @Override
    public void onImageDownloaded(ImageView imageView, Bitmap bitmap, int side) {
        imageView.setImageBitmap(bitmap);
        if (isVisible && side == LEFT_SIDE){
            mLeftSideLayout.addView(imageView);
        } else if (isVisible && side == RIGHT_SIDE){
            mRightSideLayout.addView(imageView);
        }
    }
}
