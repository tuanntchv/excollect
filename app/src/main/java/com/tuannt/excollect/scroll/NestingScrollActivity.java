package com.tuannt.excollect.scroll;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.tuannt.excollect.R;

/**
 * Comment
 *
 * @author TuanNT
 */
public class NestingScrollActivity extends AppCompatActivity{
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nesting_scroll);
    }
}
