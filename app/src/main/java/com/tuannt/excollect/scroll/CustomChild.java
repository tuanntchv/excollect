package com.tuannt.excollect.scroll;

import android.content.Context;
import android.support.v4.view.NestedScrollingChild;
import android.support.v4.view.NestedScrollingChildHelper;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

/**
 * Comment
 *
 * @author TuanNT
 */
public class CustomChild extends View implements NestedScrollingChild{
    private static final String TAG = CustomChild.class.getSimpleName();

    NestedScrollingChildHelper helper;
    public CustomChild(Context context) {
        super(context);
        helper = new NestedScrollingChildHelper(this);
        helper.setNestedScrollingEnabled(true);
    }

    public CustomChild(Context context, AttributeSet attrs) {
        super(context, attrs);
        helper = new NestedScrollingChildHelper(this);
        helper.setNestedScrollingEnabled(true);
    }

    public CustomChild(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        helper = new NestedScrollingChildHelper(this);
        helper.setNestedScrollingEnabled(true);
    }

    @Override
    public boolean dispatchNestedPreScroll(int dx, int dy, int[] consumed, int[] offsetInWindow) {
        Log.d(TAG, "dispatchNestedPreScroll: ");
        return super.dispatchNestedPreScroll(dx, dy, consumed, offsetInWindow);
    }

    @Override
    public boolean dispatchNestedScroll(int dxConsumed, int dyConsumed, int dxUnconsumed, int dyUnconsumed, int[] offsetInWindow) {
        Log.d(TAG, "dispatchNestedScroll: ");
        return super.dispatchNestedScroll(dxConsumed, dyConsumed, dxUnconsumed, dyUnconsumed, offsetInWindow);
    }


    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return super.onTouchEvent(event);
    }
}
