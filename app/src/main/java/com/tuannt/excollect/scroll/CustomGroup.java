package com.tuannt.excollect.scroll;

import android.content.Context;
import android.support.v4.view.NestedScrollingParent;
import android.util.AttributeSet;
import android.util.Log;
import android.view.ViewGroup;

/**
 * Comment
 *
 * @author TuanNT
 */
public class CustomGroup extends ViewGroup implements NestedScrollingParent {
    private static final String TAG = CustomGroup.class.getSimpleName();

    public CustomGroup(Context context) {
        super(context);
    }

    public CustomGroup(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomGroup(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onLayout(boolean b, int i, int i1, int i2, int i3) {
        Log.d(TAG, "onLayout: " + b + "/" + i + "/" + i1 + "/" + i2 + "/" + i3);
    }

    @Override
    public boolean dispatchNestedScroll(int dxConsumed, int dyConsumed, int dxUnconsumed, int dyUnconsumed, int[] offsetInWindow) {
        Log.d(TAG, "dispatchNestedScroll: ");
        return super.dispatchNestedScroll(dxConsumed, dyConsumed, dxUnconsumed, dyUnconsumed, offsetInWindow);
    }
}
