package com.tuannt.excollect.aidl2.servier;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.annotation.Nullable;

import com.tuannt.excollect.ICaculator;
import com.tuannt.excollect.ICallBack;

/**
 * Comment
 *
 * @author TuanNT
 */
public class CalculatorService extends Service {

    private ICaculator.Stub mBinder = new ICaculator.Stub() {
        @Override
        public int add(int a, int b, ICallBack callBack) throws RemoteException {
            callBack.onResult("success");
            return a + b;
        }
    };


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }
}
