package com.tuannt.excollect.aidl2.client;

import android.app.Service;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.tuannt.excollect.ICaculator;
import com.tuannt.excollect.R;

/**
 * Comment
 *
 * @author TuanNT
 */
public class CalculatorActivity extends AppCompatActivity{
    private ServiceConnection mServiceConnection;
    protected ICaculator iCaculator;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculator);

        initConnection();
    }

//    private ICallBack mICallBack = new ICallBack.Stub() {
//        @Override
//        public void onResult(String message) throws RemoteException {
//            Log.d(TAG, "onResult: callback ");
//        }
//    };

    private void initConnection() {
        mServiceConnection = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
                iCaculator = ICaculator.Stub.asInterface(iBinder);
            }

            @Override
            public void onServiceDisconnected(ComponentName componentName) {
                iCaculator = null;
            }
        };
        if (iCaculator == null) {
            Intent it = new Intent();
            it.setAction("calculator.service.hii");
            // binding to remote service
            bindService(it, mServiceConnection, Service.BIND_AUTO_CREATE);
        }
    }
}
