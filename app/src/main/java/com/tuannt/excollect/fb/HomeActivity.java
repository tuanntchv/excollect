package com.tuannt.excollect.fb;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.tuannt.excollect.R;

import java.util.Arrays;

/**
 * Comment
 *
 * @author TuanNT
 */
public class HomeActivity extends AppCompatActivity {
    private static final String[] FB_PERMISSIONS = {"user_friends"};
    private final String TAG = this.getClass().getSimpleName();
    private Button mBtnLogin;
    private Button mBtnGet;
    private CallbackManager mCallbackManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_fb);
        mBtnLogin = (Button) findViewById(R.id.mBtnLogin);
        mBtnGet = (Button) findViewById(R.id.mBtnGet);
        initCallbackFb();

        mBtnGet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new GraphRequest(
                        AccessToken.getCurrentAccessToken(),
                        "/Amthucdathanh/albums",
                        null,
                        HttpMethod.GET,
                        new GraphRequest.Callback() {
                            public void onCompleted(GraphResponse response) {
                                Log.d(TAG, "onCompleted: " + response.toString());
                            }
                        }
                ).executeAsync();
            }
        });

        mBtnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loginFb();
            }
        });
    }

    private void initCallbackFb() {
        mCallbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().registerCallback(mCallbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Log.d(TAG, "onSuccess: ");
            }

            @Override
            public void onCancel() {
                Log.d(TAG, "onCancel: ");
            }

            @Override
            public void onError(FacebookException error) {
                Log.d(TAG, "onError: ");
            }
        });
    }

    private void loginFb() {
        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList(FB_PERMISSIONS));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mCallbackManager.onActivityResult(requestCode, resultCode, data);
    }
}
