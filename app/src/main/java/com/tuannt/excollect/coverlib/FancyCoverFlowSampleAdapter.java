package com.tuannt.excollect.coverlib;

import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.tuannt.excollect.R;

/**
 * Comment
 *
 * @author TuanNT
 */
public class FancyCoverFlowSampleAdapter extends FancyCoverFlowAdapter {
    //private int[] images = {R.drawable.ic_1, R.drawable.ic_2, R.drawable.ic_3, R.drawable.ic_4, R.drawable.ic_5, R.drawable.ic_6,R.drawable.ic_1, R.drawable.ic_2, R.drawable.ic_3};
    private int[] images = {R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1, R.drawable.image1};

    // =============================================================================
    // Supertype overrides
    // =============================================================================

    @Override
    public int getCount() {
        return images.length;
    }

    @Override
    public Integer getItem(int i) {
        return images[i];
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getCoverFlowItem(int i, View reuseableView, ViewGroup viewGroup) {

        ItemHolder holder;
        if (reuseableView == null) {
            holder = new ItemHolder();
            reuseableView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_cover, null);
            holder.imgPhoto = (ImageView) reuseableView.findViewById(R.id.mImgPhoto);
            holder.mRootLayout = (RelativeLayout) reuseableView.findViewById(R.id.mRootLayout);
            reuseableView.setTag(holder);
        } else {
            holder = (ItemHolder) reuseableView.getTag();
        }

        holder.mRootLayout.setLayoutParams(new FancyCoverFlow.LayoutParams(500, 600));
        holder.mRootLayout.setBackground(ContextCompat.getDrawable(viewGroup.getContext(), R.drawable.bg_item));
        holder.imgPhoto.setImageResource(this.getItem(i));

        return reuseableView;
    }

    private class ItemHolder {
        private ImageView imgPhoto;
        private RelativeLayout mRootLayout;
    }
}
