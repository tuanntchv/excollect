package com.tuannt.excollect.service;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.tuannt.excollect.R;

/**
 * Comment
 *
 * @author TuanNT
 */
public class ServiceActivity extends AppCompatActivity implements MyResultReceive.Receiver {
    private Button mBtnNoLoop;
    private Button mBtnLoop;
    private Button mBtnBoundService;
    private Button mBtnIntentService;

    private BoundService mBoundService;
    private ServiceConnection mServiceConnection;
    private final String TAG = this.getClass().getSimpleName();
    private MyResultReceive mReceiver;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service);
        mBtnLoop = (Button) findViewById(R.id.mBtnLoop);
        mBtnNoLoop = (Button) findViewById(R.id.mBtnNoLoop);
        mBtnBoundService = (Button) findViewById(R.id.mBtnBoundService);
        mBtnIntentService = (Button) findViewById(R.id.mBtnIntentService);

        initBoundService();

        mBtnLoop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ServiceActivity.this, LoopService.class);
                startService(intent);
            }
        });

        mBtnNoLoop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ServiceActivity.this, NoLoopService.class);
                startService(intent);
            }
        });

        mBtnBoundService.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // unbindService(mServiceConnection); use to disconnect to service
                if (mBoundService != null) {
                    mBoundService.doSomeThings();
                } else {
                    Log.d(TAG, "onClick: service null");
                }
            }
        });
        mBtnIntentService.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startIntentService();
            }
        });
    }

    private void startIntentService() {
        mReceiver = new MyResultReceive(new Handler());
        mReceiver.setReceiver(this);
        Intent intent = new Intent(Intent.ACTION_SYNC, null, this, IntentService.class);

        /* Send optional extras to Download IntentService */
        intent.putExtra("message", "start");
        intent.putExtra("receiver", mReceiver);
        intent.putExtra("requestId", 101);
        startService(intent);
    }

    private void initBoundService() {
        mServiceConnection = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
                mBoundService = ((BoundService.MyBinder) iBinder).getService();
                Log.d(TAG, "onServiceConnected:");
            }

            @Override
            public void onServiceDisconnected(ComponentName componentName) {
                Log.d(TAG, "onServiceDisconnected: ");
            }
        };
        // Start service
        Intent intent = new Intent(ServiceActivity.this, BoundService.class);
        bindService(intent, mServiceConnection, Context.BIND_AUTO_CREATE);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbindService(mServiceConnection);
    }

    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {
        switch (resultCode) {
            case IntentService.STATUS_RUNNING:
                Log.d(TAG, "onReceiveResult: running");
                break;
            case IntentService.STATUS_FINISHED:
                Log.d(TAG, "onReceiveResult: finished -" + resultData.getString("result_message"));
                break;
        }
    }
}
