package com.tuannt.excollect.service;

/**
 * Comment
 *
 * @author TuanNT
 */

/**
 * @link http://javatechig.com/android/creating-a-background-service-in-android
 */
public class IntentServiceNote {

    /*IntentService is a subclass of android.app.Service class.
     A stated intent service allows to handle long running tasks without effecting the application UI thread.
     This is not bound to any activity so, it is not getting effected for any change in activity lifecycle.
     Once IntentService is started, it handles each Intent using a worker thread and stops itself when it runs out of work.*/

//    IntentService Limitations
    /*No easy or direct way to interact with user interface directly from IntentService.
     Later in this example, we will explain to pass result back from IntentService to*/

//  1  With IntentService, there can only be one request processed at any single point of time.
//  2  If you request for another task, then the new job will wait until the previous one is completed.
//     This means that IntentService process the request

//  3  An tasks stated using IntentService cannot be interrupted
}
