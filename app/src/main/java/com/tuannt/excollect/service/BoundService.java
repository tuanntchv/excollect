package com.tuannt.excollect.service;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

/**
 * Comment
 *
 * @author TuanNT
 */
public class BoundService extends Service {
    private final String TAG = this.getClass().getSimpleName();
    private MyBinder myBinder;

    @Override
    public void onCreate() {
        super.onCreate();
        myBinder = new MyBinder();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return myBinder;
    }

    public void doSomeThings() {
        Log.d(TAG, "doSomeThings: ");
    }

    class MyBinder extends Binder {
        public BoundService getService() {
            return BoundService.this;
        }
    }
}
