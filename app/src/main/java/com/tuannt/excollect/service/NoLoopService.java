package com.tuannt.excollect.service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

/**
 * Comment
 *
 * @author TuanNT
 */
public class NoLoopService extends Service {
    private static final String TAG = "result no loop";

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "onStartCommand: ");
        return Service.START_NOT_STICKY;
    }
}
