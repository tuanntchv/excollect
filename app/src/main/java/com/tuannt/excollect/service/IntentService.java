package com.tuannt.excollect.service;

import android.content.Intent;
import android.util.Log;

/**
 * Comment
 * {@link IntentServiceNote}
 *
 * @author TuanNT
 */
public class IntentService extends android.app.IntentService {
    private final String TAG = this.getClass().getSimpleName();
    public static final int STATUS_RUNNING = 0;
    public static final int STATUS_FINISHED = 1;
    public static final int STATUS_ERROR = 2;

    public IntentService() {
        super(IntentService.class.getName());
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        //TODO solution 1
/*        final ResultReceiver receiver = intent.getParcelableExtra("receiver");
        String messageReceiver = intent.getStringExtra("message");
        Log.d(TAG, "onHandleIntent: receive mesage- " + messageReceiver);
        Bundle bundle = new Bundle();
        receiver.send(STATUS_RUNNING, bundle.EMPTY);

        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            Log.d(TAG, "onHandleIntent: " + e.getMessage());
        }

        bundle.putString("result_message", "successful");
        receiver.send(STATUS_FINISHED, bundle);
        Log.d(TAG, "onHandleIntent: send finshed");*/

//        TODO solution 2
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            Log.d(TAG, "onHandleIntent: " + e.getMessage());
        }


        //TODO custom filter action
        String actionName = "com.tuannt.excollect.custom";
        Intent customIntent = new Intent();
        customIntent.setAction(actionName);
        customIntent.addCategory(Intent.CATEGORY_DEFAULT);
        sendBroadcast(customIntent);
        Log.d(TAG, "onHandleIntent: sendBroadcast finished and stop IntentService");
    }
}
