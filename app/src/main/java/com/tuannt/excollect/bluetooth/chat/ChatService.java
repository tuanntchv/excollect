package com.tuannt.excollect.bluetooth.chat;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.os.*;
import android.os.Message;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.UUID;

/**
 * Comment
 *
 * @author TuanNT
 */
public class ChatService {

    private final String TAG = this.getClass().getSimpleName();

    public enum ConnectState {
        DISCONNECT(0), CONNECTING(1), CONNECTED(3), STOP_LISTENING(4), LISTENING(4);

        private final int value;

        ConnectState(int i) {
            this.value = i;
        }

        public int getValue() {
            return value;
        }
    }

    public enum ConnectType {
        SECURE(0), INSECURE(1);

        private final int value;

        ConnectType(int i) {
            this.value = i;
        }

        public int getValue() {
            return value;
        }
    }

    // Name for the SDP record when creating server socket
    private static final String NAME_SECURE = "BluetoothChatSecure";
    private static final String NAME_INSECURE = "BluetoothChatInsecure";

    // Unique UUID for this application
    private static final UUID MY_UUID_SECURE = UUID.fromString("fa87c0d0-afac-11de-8a39-0800200c9a66");
    private static final UUID MY_UUID_INSECURE = UUID.fromString("8ce255c0-200a-11e0-ac64-0800200c9a66");

    private ConnectState mConnectState;
    private ConnectType mConnectType;
    private BluetoothAdapter mBluetoothAdapter;
    private Handler mHandler;

    private ListeningThread mListeningThread;
    private MakeConnectionThread mMakeConnectionThread;
    private ConnectedThread mConnectedThread;


    public ChatService(Handler handler) {
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        mConnectState = ConnectState.DISCONNECT;
        mHandler = handler;
        mConnectType = ConnectType.SECURE;

        startListening();
    }

    private synchronized void changeConnectState(ConnectState connectState) {
        this.mConnectState = connectState;
    }

    private synchronized void startListening() {
        //// TODO: 1/13/16 start listening
        changeConnectState(ConnectState.LISTENING);
        if (mListeningThread != null) {
            mListeningThread.cancel();
            mListeningThread = null;
        }
        if (mMakeConnectionThread != null) {
            mMakeConnectionThread.cancel();
            mMakeConnectionThread = null;
        }
        if (mConnectedThread != null) {
            mConnectedThread.cancel();
            mConnectedThread = null;
        }

        mListeningThread = new ListeningThread(mConnectType);
        mListeningThread.start();
        changeConnectState(ConnectState.LISTENING);

    }

    private synchronized void stopListening() {
        // TODO: 1/13/16 stop listening
        changeConnectState(ConnectState.STOP_LISTENING);
        if (mListeningThread != null) {
            mListeningThread.cancel();
            mListeningThread = null;
        }
    }

    public synchronized void connect(BluetoothDevice device, ConnectType connectType) {
        mConnectType = connectType;

        if (mMakeConnectionThread != null) {
            mMakeConnectionThread.cancel();
            mMakeConnectionThread = null;
        }

        if (mConnectedThread != null) {
            mConnectedThread.cancel();
            mConnectedThread = null;
        }

        mMakeConnectionThread = new MakeConnectionThread(device, connectType);
        mMakeConnectionThread.start();
        changeConnectState(ConnectState.CONNECTING);
    }

    public synchronized void connected(BluetoothSocket socket, BluetoothDevice device, ConnectType type) {
        if (mMakeConnectionThread != null) {
            mMakeConnectionThread.cancel();
            mMakeConnectionThread = null;
        }

        if (mConnectedThread != null) {
            mConnectedThread.cancel();
            mConnectedThread = null;
        }

        mConnectedThread = new ConnectedThread(socket, type);
        mConnectedThread.start();
        changeConnectState(ConnectState.CONNECTED);

        // Send the name of the connected device back to the UI Activity
        android.os.Message msg = mHandler.obtainMessage(Constants.MESSAGE_DEVICE_NAME);
        Bundle bundle = new Bundle();
        bundle.putString(Constants.DEVICE_NAME, device.getName());
        msg.setData(bundle);
        mHandler.sendMessage(msg);
        changeConnectState(ConnectState.CONNECTED);
    }

    private void connectFailed() {
        // Send a failure message back to the Activity
        Message msg = mHandler.obtainMessage(Constants.MESSAGE_TOAST);
        Bundle bundle = new Bundle();
        bundle.putString(Constants.TOAST, "Unable to connect device");
        msg.setData(bundle);
        mHandler.sendMessage(msg);

        // Start the service over to restart listening mode
        ChatService.this.startListening();
    }


    private void connectionLost() {
        // Send a failure message back to the Activity
        Message msg = mHandler.obtainMessage(Constants.MESSAGE_TOAST);
        Bundle bundle = new Bundle();
        bundle.putString(Constants.TOAST, "Device connection was lost");
        msg.setData(bundle);
        mHandler.sendMessage(msg);
        // Start the service over to restart listening mode
        ChatService.this.startListening();
    }

    public void write(byte[] out) {
        // Create temporary object
        ConnectedThread r;
        // Synchronize a copy of the ConnectedThread
        synchronized (this) {
            if (mConnectState != ConnectState.CONNECTED) return;
            r = mConnectedThread;
        }
        // Perform the write unsynchronized
        r.write(out);
    }

    class ListeningThread extends Thread {
        private BluetoothServerSocket mServerSocket;
        private ConnectType mConnectType;

        public ListeningThread(ConnectType mConnectType) {
            this.mConnectType = mConnectType;

            BluetoothServerSocket tmp = null;
            // Create a new listening server socket
            try {
                if (mConnectType == ConnectType.SECURE) {
                    tmp = mBluetoothAdapter.listenUsingRfcommWithServiceRecord(NAME_SECURE, MY_UUID_SECURE);
                } else {
                    tmp = mBluetoothAdapter.listenUsingInsecureRfcommWithServiceRecord(NAME_INSECURE, MY_UUID_INSECURE);
                }
            } catch (IOException e) {
                Log.e(TAG, "ListeningThread", e);
            }
            this.mServerSocket = tmp;
        }

        @Override
        public void run() {
            BluetoothSocket bluetoothSocket = null;
            while (mConnectState != ConnectState.STOP_LISTENING) {
                try {
                    bluetoothSocket = mServerSocket.accept();
                    Log.d(TAG, "run: listening again");
                } catch (IOException e) {
                    Log.d(TAG, "ListeningThread run: ", e);
                }

                if (bluetoothSocket != null) {
                    synchronized (ChatService.this) {
                        connected(bluetoothSocket, bluetoothSocket.getRemoteDevice(), mConnectType);
                        Log.d(TAG, "run: connect is comming");
                    }
                }
            }
        }

        public void cancel() {
            try {
                mServerSocket.close();
            } catch (IOException e) {
                Log.e(TAG, "Socket close() of server failed", e);
            }
        }
    }

    class MakeConnectionThread extends Thread {
        private final BluetoothSocket mSocket;
        private final BluetoothDevice mDevice;
        private ConnectType mConnectType;

        public MakeConnectionThread(BluetoothDevice device, ConnectType connectType) {
            this.mConnectType = connectType;
            this.mDevice = device;

            BluetoothSocket tmp = null;
            try {
                if (mConnectType == ConnectType.SECURE) {
                    tmp = mDevice.createRfcommSocketToServiceRecord(MY_UUID_SECURE);
                } else {
                    tmp = mDevice.createInsecureRfcommSocketToServiceRecord(MY_UUID_INSECURE);
                }
            } catch (IOException e) {
                Log.e(TAG, "MakeConnectionThread", e);
            }
            mSocket = tmp;
        }

        @Override
        public void run() {
            mBluetoothAdapter.cancelDiscovery();
            try {
                mSocket.connect();
            } catch (IOException e) {
                try {
                    mSocket.close();
                } catch (IOException e1) {
                    Log.d(TAG, "MakeConnectionThread run: close socket ", e);
                }
                connectFailed();
                Log.d(TAG, "MakeConnectionThread- run: ", e);
                return;
            }
            // Reset the ConnectThread because we're done
            synchronized (ChatService.this) {
                mMakeConnectionThread = null;
            }
            connected(mSocket, mDevice, mConnectType);
        }

        public void cancel() {
            try {
                mSocket.close();
            } catch (IOException e) {
                Log.e(TAG, "close() of connect " + " socket failed", e);
            }
        }
    }


    class ConnectedThread extends Thread {
        private final BluetoothSocket mSocket;
        private final InputStream mInStream;
        private final OutputStream mOutStream;

        public ConnectedThread(BluetoothSocket socket, ConnectType connectType) {
            Log.d(TAG, "create ConnectedThread: ");
            mSocket = socket;
            InputStream tmpIn = null;
            OutputStream tmpOut = null;

            // Get the BluetoothSocket input and output streams
            try {

                tmpIn = mSocket.getInputStream();
                tmpOut = mSocket.getOutputStream();
            } catch (IOException e) {
                Log.e(TAG, "temp sockets not created", e);
            }

            mInStream = tmpIn;
            mOutStream = tmpOut;
        }

        public void run() {
            Log.i(TAG, "BEGIN mConnectedThread");
            byte[] buffer = new byte[1024];
            int bytes;

            // Keep listening to the InputStream while connected
            while (true) {
                try {
                    // Read from the InputStream
                    bytes = mInStream.read(buffer);

                    // Send the obtained bytes to the UI Activity
                    mHandler.obtainMessage(Constants.MESSAGE_READ, bytes, -1, buffer).sendToTarget();
                } catch (IOException e) {
                    Log.e(TAG, "disconnected", e);
                    connectionLost();
                    // Start the service over to restart listening mode
                    ChatService.this.startListening();
                    return;
                }
            }
        }

        /**
         * Write to the connected OutStream.
         *
         * @param buffer The bytes to write
         */
        public void write(byte[] buffer) {
            try {
                mOutStream.write(buffer);

                // Share the sent message back to the UI Activity
                mHandler.obtainMessage(Constants.MESSAGE_WRITE, -1, -1, buffer)
                        .sendToTarget();
            } catch (IOException e) {
                Log.e(TAG, "Exception during write", e);
            }
        }

        public void cancel() {
            try {
                mSocket.close();
            } catch (IOException e) {
                Log.e(TAG, "close() of connect socket failed", e);
            }
        }
    }

}
