package com.tuannt.excollect.bluetooth.chat;

import android.app.Dialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.tuannt.excollect.R;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Comment
 *
 * @author TuanNT
 */
public class ChatActivity extends AppCompatActivity implements View.OnClickListener {
    private final String TAG = this.getClass().getSimpleName();
    private List<Message> mMessage = new ArrayList<>();
    private List<Client> mClients = new ArrayList<>();
    private MessageAdapter mAdapter;
    private BluetoothAdapter mBluetoothAdapter;
    private ClientAdapter mClientAdapter;

    private ChatService mChatService;


    private RecyclerView mRecyclerView;
    private Button mBtnSend;
    private Button mBtnSearch;
    private EditText mEdtInput;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        mRecyclerView = (RecyclerView) findViewById(R.id.mRecyclerView);
        mBtnSend = (Button) findViewById(R.id.mBtnSend);
        mBtnSearch = (Button) findViewById(R.id.mBtnSearch);
        mEdtInput = (EditText) findViewById(R.id.mEdtInput);

        mBtnSend.setOnClickListener(this);
        mBtnSearch.setOnClickListener(this);
        initRecyclerView();
        initBluetooth();
        initClientAdapter();

        getPairedDevices();

        mChatService = new ChatService(mHandler);
    }

    private void initClientAdapter() {
        mClientAdapter = new ClientAdapter(mClients, null);
    }

    private void initBluetooth() {
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (mBluetoothAdapter == null) {
            Log.d(TAG, "onCreate: device unsupport");
        }
    }

    private void initRecyclerView() {
        mAdapter = new MessageAdapter(mMessage);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setAdapter(mAdapter);
    }

    private void startDiscover() {
        IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
        registerReceiver(mDiscoverDeviceReceive, filter);
        mBluetoothAdapter.startDiscovery();
    }

    private void stopDiscover() {
        mBluetoothAdapter.cancelDiscovery();
        unregisterReceiver(mDiscoverDeviceReceive);
    }

    private final BroadcastReceiver mDiscoverDeviceReceive = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                Log.d(TAG, "onReceive: found device");
                // Get the BluetoothDevice object from the Intent
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                // Add the name and address to an array adapter to show in a ListView
                Client client = new Client();
                client.setName(device.getName() + " " + device.getAddress());
                client.setDevice(device );
                mClients.add(client);
                mClientAdapter.notifyDataSetChanged();
            }
        }
    };

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.mBtnSearch:
                showDialogClients();
                break;
            case R.id.mBtnSend:
                // Get the message bytes and tell the BluetoothChatService to write
                byte[] send = mEdtInput.getText().toString().getBytes();
                mChatService.write(send);
                break;
        }
    }

    private void getPairedDevices() {
        Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();
        // If there are paired devices
        if (pairedDevices.size() > 0) {
            // Loop through paired devices
            for (BluetoothDevice device : pairedDevices) {
                Client client = new Client();
                client.setName(device.getName() + " " + device.getAddress());
                client.setDevice(device);
                mClients.add(client);
            }
        }
    }

    private void showDialogClients() {
        startDiscover();
        final Dialog dialog = new Dialog(this, R.style.TranparentDialog);
        View view = LayoutInflater.from(this).inflate(R.layout.dialog_client, null);
        dialog.setContentView(view);
        dialog.setCanceledOnTouchOutside(false);

        mClientAdapter.setmListener(new ClientAdapter.OnClientItemClick() {
            @Override
            public void onClientItemClick(int position) {
                dialog.dismiss();
                stopDiscover();
                connectToClient(mClients.get(position));
            }
        });
        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.mRecyclerView);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(ChatActivity.this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(mClientAdapter);
        dialog.show();
    }

    private void connectToClient(Client client) {
        Log.d(TAG, "connectToClient: " + (client.getDevice() == null));
        mChatService.connect(client.getDevice(), ChatService.ConnectType.SECURE);
    }

    private final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(android.os.Message msg) {
            switch (msg.what) {
//                case Constants.MESSAGE_STATE_CHANGE:
//                    switch (msg.arg1) {
//                        case ChatService.ConnectState.CONNECTED:
//                            setStatus(getString(R.string.title_connected_to, mConnectedDeviceName));
//                            mConversationArrayAdapter.clear();
//                            break;
//                        case BluetoothChatService.STATE_CONNECTING:
//                            setStatus(R.string.title_connecting);
//                            break;
//                        case BluetoothChatService.STATE_LISTEN:
//                        case BluetoothChatService.STATE_NONE:
//                            setStatus(R.string.title_not_connected);
//                            break;
//                    }
//                    break;
                case Constants.MESSAGE_WRITE:
                    byte[] writeBuf = (byte[]) msg.obj;
                    // construct a string from the buffer
                    String writeMessage = new String(writeBuf);
                    Log.d(TAG, "handleMessage: write" + writeMessage);
                    break;
                case Constants.MESSAGE_READ:
                    byte[] readBuf = (byte[]) msg.obj;
                    // construct a string from the valid bytes in the buffer
                    String readMessage = new String(readBuf, 0, msg.arg1);
                    Log.d(TAG, "handleMessage: read" + readMessage);
                    break;
                case Constants.MESSAGE_DEVICE_NAME:
                    // save the connected device's name
                    String mConnectedDeviceName = msg.getData().getString(Constants.DEVICE_NAME);
                    Toast.makeText(ChatActivity.this, mConnectedDeviceName, Toast.LENGTH_SHORT).show();
                    break;
                case Constants.MESSAGE_TOAST:
                    Toast.makeText(ChatActivity.this, msg.getData().getString(Constants.TOAST), Toast.LENGTH_SHORT).show();
                    break;
            }
        }
    };
}
