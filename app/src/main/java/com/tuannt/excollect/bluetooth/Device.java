package com.tuannt.excollect.bluetooth;

import android.bluetooth.BluetoothDevice;

/**
 * Comment
 *
 * @author TuanNT
 */
public class Device {
    private String name;
    private boolean isSelected;
    private BluetoothDevice mDevice;

    public Device(String name, boolean isSelected) {
        this.name = name;
        this.isSelected = isSelected;
    }

    public Device() {
    }

    public BluetoothDevice getDevice() {
        return mDevice;
    }

    public void setDevice(BluetoothDevice mDevice) {
        this.mDevice = mDevice;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}
