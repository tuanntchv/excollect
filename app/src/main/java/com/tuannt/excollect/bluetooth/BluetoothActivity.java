package com.tuannt.excollect.bluetooth;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.tuannt.excollect.R;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;


/**
 * Comment
 *
 * @author TuanNT
 */
public class BluetoothActivity extends AppCompatActivity implements View.OnClickListener, DeviceAdapter.OnDeviceListener {
    private static final int REQUEST_ENABLE_BT = 100;
    private final String TAG = this.getClass().getSimpleName();
    private Button mBtnOff;
    private Button mBtnOn;
    private Button mBtnSearch;
    private RecyclerView mRecyclerView;

    private DeviceAdapter mAdapter;
    private BluetoothAdapter mBluetoothAdapter;
    private List<Device> mDevices = new ArrayList<>();


    private boolean mIsSearching = false;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bluetooth);
        mBtnOff = (Button) findViewById(R.id.mBtnTurnOf);
        mBtnOn = (Button) findViewById(R.id.mBtnTurnOn);
        mBtnSearch = (Button) findViewById(R.id.mBtnSearch);
        mRecyclerView = (RecyclerView) findViewById(R.id.mRecyclerView);

        initRecyclerView();

        mBtnOff.setOnClickListener(this);
        mBtnOn.setOnClickListener(this);
        mBtnSearch.setOnClickListener(this);
        // Register bluetooth state change
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (mBluetoothAdapter == null) {
            Log.d(TAG, "onCreate: device unsupport");
        }
        IntentFilter filter = new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED);
        registerReceiver(mBluetoothStateReceive, filter);
    }

    private void initRecyclerView() {
        getPairedDevices();
        mAdapter = new DeviceAdapter(mDevices, this);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setAdapter(mAdapter);
    }

    private void getPairedDevices() {
        Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();
        // If there are paired devices
        if (pairedDevices.size() > 0) {
            // Loop through paired devices
            for (BluetoothDevice device : pairedDevices) {
                Log.d(TAG, "getPairedDevices: ");
                // Add the name and address to an array adapter to show in a ListView
                Device device1 = new Device(device.getName() + " " + device.getAddress(), true);
                device1.setDevice(device);
                mDevices.add(device1);
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(mBluetoothStateReceive);
        unregisterReceiver(mDiscoverDeviceReceive);
    }

    private final BroadcastReceiver mDiscoverDeviceReceive = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                Log.d(TAG, "onReceive: found device");
                // Get the BluetoothDevice object from the Intent
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                // Add the name and address to an array adapter to show in a ListView
                Device mDevice = new Device(device.getName() + " " + device.getAddress(), false);
                mDevice.setDevice(device);
                mDevices.add(mDevice);
                mAdapter.notifyDataSetChanged();
            }
        }
    };

    private final BroadcastReceiver mBluetoothStateReceive = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (!action.equals(BluetoothAdapter.ACTION_STATE_CHANGED)) {
                return;
            }
            int state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, BluetoothAdapter.ERROR);
            switch (state) {
                case BluetoothAdapter.STATE_OFF:
                    Log.d(TAG, "onReceive: off");
                    break;
                case BluetoothAdapter.STATE_ON:
                    Log.d(TAG, "onReceive: on");
                    break;
                case BluetoothAdapter.STATE_TURNING_OFF:
                    Log.d(TAG, "onReceive: turning off");
                    break;
                case BluetoothAdapter.STATE_TURNING_ON:
                    Log.d(TAG, "onReceive: turning on");
                    break;
            }
        }
    };

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.mBtnSearch:
                if (mIsSearching) {
                    stopDiscoverDevices();
                } else {
                    startDiscoverDevices();
                }

                break;
            case R.id.mBtnTurnOf:
                break;
            case R.id.mBtnTurnOn:
                if (!mBluetoothAdapter.isEnabled()) {
                    Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                    startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
                }
                break;
        }
    }

    private void unpairDevice(BluetoothDevice device) {
        try {
            Method method = device.getClass().getMethod("removeBond", (Class[]) null);
            method.invoke(device, (Object[]) null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void pairDevice(BluetoothDevice device) {
        try {
            Method method = device.getClass().getMethod("createBond", (Class[]) null);
            method.invoke(device, (Object[]) null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void startDiscoverDevices() {
        Log.d(TAG, "startDiscoverDevices: start discover");
        mIsSearching = true;
        IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
        registerReceiver(mDiscoverDeviceReceive, filter);
        mBluetoothAdapter.startDiscovery();
    }


    private void stopDiscoverDevices() {
        Log.d(TAG, "stopDiscoverDevices: stop discover");
        unregisterReceiver(mDiscoverDeviceReceive);
        mIsSearching = false;
        mBluetoothAdapter.cancelDiscovery();
    }

    private void enableDiscoverAbility() {
        Intent discoverableIntent = new
                Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
        discoverableIntent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 300);
        startActivity(discoverableIntent);

        // implement result on onActivityResult()
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_ENABLE_BT || resultCode == RESULT_OK) {
            Log.d(TAG, "onActivityResult: bluetooth enabled");
        }
    }

    @Override
    public void onDeviceItemClick(int position) {
        BluetoothDevice bluetoothDevice = mDevices.get(position).getDevice();
        if (bluetoothDevice.getBondState() == BluetoothDevice.BOND_NONE) {
            Log.d(TAG, "pairDevice: ");
            pairDevice(bluetoothDevice);
        } else {
            Log.d(TAG, "unpairDevice: ");
            unpairDevice(bluetoothDevice);
        }
    }
}
