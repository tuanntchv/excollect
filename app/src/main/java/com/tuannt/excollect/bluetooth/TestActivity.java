package com.tuannt.excollect.bluetooth;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import com.tuannt.excollect.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Comment
 *
 * @author TuanNT
 */
public class TestActivity extends AppCompatActivity {

    private final String TAG = this.getClass().getSimpleName();
    private RecyclerView mRecyclerView;
    private DeviceAdapter mAdapter;
    private List<Device> mDevices = new ArrayList<>();

    private Button mBtnDown;
    private Button mBtnUp;
    private LinearLayout mContainer;

    private int mCurrentIdViewFocus;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bluetooth);
        mRecyclerView = (RecyclerView) findViewById(R.id.mRecyclerView);
        mBtnDown = (Button) findViewById(R.id.mBtnDown);
        mBtnUp = (Button) findViewById(R.id.mBtnUp);
        mContainer = (LinearLayout) findViewById(R.id.mContainer);

        initRecyclerView();


        mBtnDown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int currentSelectd = getCurrentSelected();
                Log.d(TAG, "onKeyDown:" + currentSelectd);
                mDevices.get(currentSelectd).setSelected(false);

                currentSelectd++;
                if (currentSelectd >= mDevices.size()) {
                    mCurrentIdViewFocus = 0;
                    mContainer.getChildAt(mCurrentIdViewFocus).requestFocus();
                    mCurrentIdViewFocus++;
                } else {
                    mDevices.get(currentSelectd).setSelected(true);
                }
                mAdapter.notifyDataSetChanged();
            }
        });
        mBtnUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int currentSelectd = getCurrentSelected();
                Log.d(TAG, "onKeyUp: " + currentSelectd);
                mDevices.get(currentSelectd).setSelected(false);

                currentSelectd--;
                if (currentSelectd < 0) {
                    mCurrentIdViewFocus = mContainer.getChildCount() - 1;
                    mContainer.getChildAt(mCurrentIdViewFocus).setSelected(true);
                    if (mCurrentIdViewFocus < mContainer.getChildCount() - 2) {
                        mContainer.getChildAt(mCurrentIdViewFocus - 1).setSelected(false);
                    }

                    mCurrentIdViewFocus--;
                    // TODO: 1/8/16 move top view
                } else {
                    mDevices.get(currentSelectd).setSelected(true);
                }
                mAdapter.notifyDataSetChanged();

            }
        });
    }

    private void initRecyclerView() {
        for (int i = 0; i < 20; i++) {
            Device device = new Device("device -" + (i + 1), false);
            if (i == 0) {
                device.setSelected(true);
            }
            mDevices.add(device);
        }
        mAdapter = new DeviceAdapter(mDevices, null);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setAdapter(mAdapter);
    }

    private int getCurrentSelected() {
        for (int i = 0; i < mDevices.size(); i++) {
            Device device = mDevices.get(i);
            if (device.isSelected()) {
                return i;
            }
        }
        return 0;
    }
}
