package com.tuannt.excollect.bluetooth.chat;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tuannt.excollect.R;

import java.util.List;

/**
 * Comment
 *
 * @author TuanNT
 */
public class MessageAdapter extends RecyclerView.Adapter {

    private List<Message> messages;

    public MessageAdapter(List<Message> messages) {
        this.messages = messages;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_client, parent, false);
        return new MessageHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((MessageHolder) holder).tvMessage.setText(messages.get(position).getMessage());
    }

    @Override
    public int getItemCount() {
        return messages.size();
    }

    private class MessageHolder extends RecyclerView.ViewHolder {
        private final TextView tvMessage;

        public MessageHolder(View itemView) {
            super(itemView);
            tvMessage = (TextView) itemView;
        }
    }
}
