package com.tuannt.excollect.bluetooth.chat;

import android.bluetooth.BluetoothDevice;

/**
 * Comment
 *
 * @author TuanNT
 */
public class Client {
    private String name;
    private BluetoothDevice device;

    public Client(String name, BluetoothDevice device) {
        this.name = name;
        this.device = device;
    }

    public Client() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BluetoothDevice getDevice() {
        return device;
    }

    public void setDevice(BluetoothDevice device) {
        this.device = device;
    }
}
