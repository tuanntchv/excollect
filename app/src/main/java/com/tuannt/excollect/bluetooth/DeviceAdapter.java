package com.tuannt.excollect.bluetooth;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.tuannt.excollect.R;

import java.util.List;

/**
 * Comment
 *
 * @author TuanNT
 */
public class DeviceAdapter extends RecyclerView.Adapter {
    public interface OnDeviceListener {
        void onDeviceItemClick(int position);
    }

    private List<Device> mDevices;
    private OnDeviceListener mListener;

    public DeviceAdapter(List<Device> device, OnDeviceListener l) {
        this.mDevices = device;
        this.mListener = l;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_device, parent, false);
        return new DeviceHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        DeviceHolder deviceHolder = (DeviceHolder) holder;
        deviceHolder.tvName.setText(mDevices.get(position).getName());
        deviceHolder.rootView.setSelected(mDevices.get(position).isSelected());
    }

    @Override
    public int getItemCount() {
        return mDevices.size();
    }

    private class DeviceHolder extends RecyclerView.ViewHolder {
        private final TextView tvName;
        private final RelativeLayout rootView;

        public DeviceHolder(View itemView) {
            super(itemView);
            tvName = (TextView) itemView.findViewById(R.id.mTvName);
            rootView = (RelativeLayout) itemView.findViewById(R.id.mRootView);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mListener.onDeviceItemClick(getAdapterPosition());
                }
            });
        }
    }
}
