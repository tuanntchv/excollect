package com.tuannt.excollect.bluetooth.chat;

/**
 * Comment
 *
 * @author TuanNT
 */
public class Message {
    private String message;
    private Client client;

    public Message(String message, Client client) {
        this.message = message;
        this.client = client;
    }

    public Message() {
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }
}
