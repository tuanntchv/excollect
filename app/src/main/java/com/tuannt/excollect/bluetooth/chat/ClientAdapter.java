package com.tuannt.excollect.bluetooth.chat;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tuannt.excollect.R;

import java.util.List;

/**
 * Comment
 *
 * @author TuanNT
 */
public class ClientAdapter extends RecyclerView.Adapter {
    public interface OnClientItemClick {
        void onClientItemClick(int position);
    }

    private List<Client> clients;
    private OnClientItemClick mListener;

    public ClientAdapter(List<Client> clients, OnClientItemClick l) {
        this.clients = clients;
        this.mListener = l;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_client, parent, false);
        return new MessageHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((MessageHolder) holder).tvMessage.setText(clients.get(position).getName());
    }

    @Override
    public int getItemCount() {
        return clients.size();
    }

    private class MessageHolder extends RecyclerView.ViewHolder {
        private final TextView tvMessage;

        public MessageHolder(View itemView) {
            super(itemView);
            tvMessage = (TextView) itemView;
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mListener.onClientItemClick(getLayoutPosition());
                }
            });
        }
    }

    public void setmListener(OnClientItemClick mListener) {
        this.mListener = mListener;
    }
}
